﻿using System;
using System.Collections.Generic;
using PNetC;

namespace PNet.TestNetwork
{
    public class TestClientDispatchClient : ADispatchClient
    {
        internal PNetS.Player DispatchPlayer;

        readonly Queue<NetMessage> _messages = new Queue<NetMessage>();
        public readonly Guid Guid = Guid.NewGuid();

        public TestDispatchServer Server { get; set; }

        protected override void Start()
        {
            Server.BeginConnecting(this);
        }

        protected override void ReadQueue()
        {
            NetMessage[] messages;
            lock (_messages)
            {
                messages = _messages.ToArray();
                _messages.Clear();
            }
            foreach(var msg in messages)
                ConsumeData(msg);
        }

        protected override void Disconnect(string reason)
        {
            Server.Internal_Disconnect(this, reason);
        }

        protected override NetMessage GetMessage(int size)
        {
            return NetMessage.GetMessage(size);
        }

        protected override void InternalSendMessage(NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = GetMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);
            Server.ReceiveMessage(this, lmsg);
        }

        protected override void DisconnectIfStillConnected()
        {
            Server.Internal_Disconnect(this, "");
        }

        internal void Disconnected(string reason)
        {
            Debug.Log("Disconnected: {0}", reason);

            var lastStatus = Client.Server.Status;

            switch (lastStatus)
            {
                case ConnectionStatus.Disconnecting:
                case ConnectionStatus.Connected:
                    FinalizeDisconnect();
                    break;
                default:
                    RaiseFailedToConnect("failed to connect");
                    break;
            }
        }

        internal void ReceiveMessage(NetMessage msg)
        {
            lock(_messages)
                _messages.Enqueue(msg);
        }

        internal void AllowConnect(PNetS.Player player)
        {
            DispatchPlayer = player;
            Server.FinishConnecting(this);
            ConnectedToServer(player.Id);
        }
    }
}
