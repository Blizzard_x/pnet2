﻿using UnityEngine;

/// <summary>
/// Simple class to override instead of monobehaviour, has some extra network functions
/// </summary>
public class NetBehaviour : MonoBehaviour
{
    PNetU.NetworkView _netView;

    /// <summary>
    /// Get the PNetU.NetworkView attached to the gameObject
    /// </summary>
    public PNetU.NetworkView NetView
    {
// ReSharper disable once ConvertConditionalTernaryToNullCoalescing
        //unity doesn't support ternary with components...
        get { return _netView != null ? _netView : (_netView = GetComponent<PNetU.NetworkView>()); }
        internal set
        {
            if (_netView != null)
            {
                _netView.OnFinishedCreation -= OnFinishedCreating;
            }
            _netView = value;
            if (_netView != null)
            {
                _netView.OnFinishedCreation += OnFinishedCreating;
            }
        }
    }
    /// <summary>
    /// Called once the network view has finished attaching and instantiating
    /// </summary>
    protected virtual void OnFinishedCreating() { }
}