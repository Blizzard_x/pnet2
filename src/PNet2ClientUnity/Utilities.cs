﻿using PNet;
using UnityEngine;
using NetworkStateSynchronization = PNet.NetworkStateSynchronization;

namespace PNetU
{
    static class Utilities
    {
        public static NetworkStateSynchronization ToPNet(this UnityEngine.NetworkStateSynchronization state)
        {
            switch (state)
            {
                case UnityEngine.NetworkStateSynchronization.Off:
                    return NetworkStateSynchronization.Off;
                case UnityEngine.NetworkStateSynchronization.ReliableDeltaCompressed:
                    return NetworkStateSynchronization.ReliableDeltaCompressed;
                case UnityEngine.NetworkStateSynchronization.Unreliable:
                    return NetworkStateSynchronization.Unreliable;
                default:
                    return NetworkStateSynchronization.Off;
            }
        }

        public static RpcMode ToPNet(this RPCMode mode)
        {
            switch (mode)
            {
                case RPCMode.All:
                    return RpcMode.AllOrdered;
                case RPCMode.AllBuffered:
                    return RpcMode.AllBuffered;
                case RPCMode.Others:
                    return RpcMode.OthersOrdered;
                case RPCMode.OthersBuffered:
                    return RpcMode.OthersBuffered;
                case RPCMode.Server:
                    return RpcMode.ServerOrdered;
                default:
                    return RpcMode.ServerOrdered;
            }
        }
    }
}
